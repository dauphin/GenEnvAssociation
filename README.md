# Analysing genotype-environment association

This repository provides codes and explanations on how to perform genotype-environment association analysis using latent factor mixed model (LFMM) and discriminant redundancy analysis (RDA). Examples are provides using genomic datasets generated by the GenTree consortium https://www.gentree-h2020.eu

# References

* Frichot, E., & François, O. (2015). LEA: An R package for landscape and ecological association studies. Methods in Ecology and Evolution, 6(8), 925-929.
* Caye, K., Jumentier, B., Lepeule, J., & François, O. (2019). LFMM 2: fast and accurate inference of gene-environment associations in genome-wide studies. Molecular biology and evolution, 36(4), 852-860.
* Gain, C., & François, O. (2021). LEA 3: Factor models in population genetics and ecological genomics with R. Molecular Ecology Resources, 21(8), 2738-2748.
* Forester, B. R., Lasky, J. R., Wagner, H. H., & Urban, D. L. (2018). Comparing methods for detecting multilocus adaptation with multivariate genotype–environment associations. Molecular Ecology, 27(9), 2215-2233.
* Capblancq, T., & Forester, B. R. (2021). Redundancy analysis: A Swiss Army Knife for landscape genomics. Methods in Ecology and Evolution, 12(12), 2298-2309.

# Key topics covered

* ...

# Acknowledgements 

This material is part of the **Genomics of Environmental Adaptation** winter school and draws on the expertise of all instructors and lecturers.

# Support

Please, send an email to benjamin.dauphin@wsl.ch or christian.rellstab@wsl.ch with a reproducible example if a problem occurs when running the scripts.

# Roadmap

Future work will focus on integrating complementary R-based codes and tools to analyse genotype-environment association.

# Contributing

Any contribution or suggestion to improve or supplement this project is welcome. Please open an issue to discuss what you would like to change or contact us directly via email.

# License

GNU AGPLv3.

# Project status

This project is primarily used for teaching purposes and is therefore updated on a non-regular basis.
